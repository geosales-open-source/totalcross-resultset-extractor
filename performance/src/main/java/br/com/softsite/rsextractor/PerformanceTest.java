package br.com.softsite.rsextractor;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.totalcross.util.compile.AvailablePlatforms;
import com.totalcross.util.compile.CompilationBuilder;

import totalcross.TotalCrossApplication;
import totalcross.sql.Connection;
import totalcross.sql.DriverManager;
import totalcross.sql.ResultSet;
import totalcross.sql.Statement;
import totalcross.sys.Settings;
import totalcross.sys.Vm;
import totalcross.ui.Button;
import totalcross.ui.MainWindow;

public class PerformanceTest extends MainWindow {

	public static class Pair<K, V> {
        public K k;
        public V v;

        public Pair(K k, V v) {
            this.k = k;
            this.v = v;
        }
    }

	public static final String QUERY = "WITH RECURSIVE q AS ( " +
	        " SELECT 1 as c, 'raiz' as str " +
	        " UNION ALL " +
	        " SELECT c + 1 as c, 'derivado ' || c as str " +
	        " FROM q " +
	        " WHERE c < 100*1000 " +
	        ")" +
	        "SELECT * FROM q";

	public static void main(String[] args) throws IOException, InterruptedException {
		System.out.println("args length " + args.length);
		for (String s: args) {
			System.out.println("args " + s);
		}
		if (args.length > 0) {
			boolean compile = true;
			boolean dryRun = false;
			switch (args[0]) {
			case "-n":
				dryRun = true;
				break;
			case "compile":
				break;
			case "run":
				compile = false;
				break;
			default:
				throw new RuntimeException("Option " + args[0] + " is invalid");
			}
			if (compile) {
				new CompilationBuilder()
						.setKey(CompilationBuilder.getenv("TCKEY"))
						.setPlatformsTarget(AvailablePlatforms.WIN32)
						.setMustCompile(s -> s.replace('\\', '/').matches("^.*/totalcross-resultset-extractor/.*/totalcross-resultset-extractor-[^/]*\\.jar$"))
						.setDryRun(dryRun)
						.setMainTarget(PerformanceTest.class)
						.build();
				return;
			}
		}
		TotalCrossApplication.run(PerformanceTest.class);
	}

	@Override
	public void initUI() {
		super.initUI();
		Button btnRsExtract = new Button("test rsextract");
		add(btnRsExtract, LEFT, BOTTOM, FILL, PREFERRED);
		btnRsExtract.addPressListener(__ -> {
			try {
				runTestRSExtract();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
		Button btnRsDefault = new Button("test default");
		add(btnRsDefault, LEFT, BEFORE, FILL, PREFERRED);
		btnRsDefault.addPressListener(__ -> {
			try {
				runTestDefault();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}

	private void runTestRSExtract() throws SQLException {
		runTest("test RSExtract", rs -> RSExtract.<Pair<String, Integer>>extractData(rs, (rs_, lineno) -> new Pair<>(rs_.getString("str"), rs_.getInt("c"))));
	}

	private void runTestDefault() throws SQLException {
		runTest("test default", new RowMapperResultSetExtractor<>((rs, lineno) -> new Pair<>(rs.getString("str"), rs.getInt("c"))));
	}

	private void runTest(String test, ResultSetExtractor<List<Pair<String, Integer>>> rse) throws SQLException {
		Connection conn = null;
		try {
			System.out.println("Running test " + test);
			conn = DriverManager.getConnection("jdbc:sqlite::memory:");
			try (Statement stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery(QUERY)) {
				int init = Vm.getTimeStamp();
				int gcInitTime = Settings.gcTime;
				int gcInitCount = Settings.gcCount;
				List<Pair<String, Integer>> list = rse.extractData(rs);
				int fim = Vm.getTimeStamp();
				int gcFimTime = Settings.gcTime;
				int gcFimCount = Settings.gcCount;

				int deltaTempo = fim - init;
				int deltaGcTime = gcFimTime - gcInitTime;
				int deltaGcCount = gcFimCount - gcInitCount;
				System.out.println("\telementos " + list.size());
				System.out.println("\tdelta tempo " + deltaTempo);
				System.out.println("\tdelta gc tempo " + deltaGcTime);
				System.out.println("\tdelta gc count " + deltaGcCount);
				System.out.println();
			}
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}

}
