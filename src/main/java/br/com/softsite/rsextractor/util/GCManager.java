package br.com.softsite.rsextractor.util;

import totalcross.sys.Vm;

public class GCManager implements AutoCloseable {

	public GCManager() {
		Vm.tweak(Vm.TWEAK_DISABLE_GC, true);
	}

	public void twinkleGC() {
		Vm.tweak(Vm.TWEAK_DISABLE_GC, false);
		Vm.tweak(Vm.TWEAK_DISABLE_GC, true);
	}

	@Override
	public void close() {
		Vm.tweak(Vm.TWEAK_DISABLE_GC, false);
	}
}
