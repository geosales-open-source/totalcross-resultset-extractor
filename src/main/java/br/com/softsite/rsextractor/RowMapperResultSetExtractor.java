package br.com.softsite.rsextractor;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import totalcross.sql.ResultSet;

public class RowMapperResultSetExtractor<T> implements ResultSetExtractor<List<T>> {

	private final RowMapper<T> rowMapper;

	public RowMapperResultSetExtractor(RowMapper<T> rowMapper) {
		this.rowMapper = rowMapper;
	}

	@Override
	public List<T> extractData(ResultSet rs) throws SQLException {
		ArrayList<T> l = new ArrayList<>();
		int lineno = 0;
		while (rs.next()) {
			l.add(rowMapper.mapRow(rs, lineno));
			lineno++;
		}
		return l;
	}

}
