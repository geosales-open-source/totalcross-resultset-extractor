package br.com.softsite.rsextractor;

import java.sql.SQLException;

import totalcross.sql.ResultSet;

@FunctionalInterface
public interface ResultSetExtractor<T> {

	T extractData(ResultSet rs) throws SQLException;
}
