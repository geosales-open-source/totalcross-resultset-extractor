package br.com.softsite.rsextractor;

import java.sql.SQLException;

import totalcross.sql.ResultSet;

@FunctionalInterface
public interface RowMapper<T> {

	T mapRow(ResultSet rs, int rowNum) throws SQLException;
}
