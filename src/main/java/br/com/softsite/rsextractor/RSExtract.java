package br.com.softsite.rsextractor;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.softsite.rsextractor.util.GCManager;
import totalcross.sql.ResultSet;

public class RSExtract {

	public static final int GC_AFTER_ITERATIONS_DEFAULT = 50_000;
	private static int gcAfterInterations = GC_AFTER_ITERATIONS_DEFAULT;

	public static <T> List<T> extractData(ResultSet rs, RowMapper<T> mapper) throws SQLException {
		return extractData(rs, gcAfterInterations, mapper);
	}

	public static <T> List<T> extractData(ResultSet rs, final int interval, RowMapper<T> mapper) throws SQLException {
		ArrayList<T> result = new ArrayList<>();
		int i = 0;
		int line = 0;
		try (GCManager gcm = new GCManager()) {
			while (rs.next()) {
				result.add(mapper.mapRow(rs, line));
				i++;
				line++;
				if (i == interval) {
					i = 0;
					gcm.twinkleGC();
				}
			}
		}
		return result;
	}
}
